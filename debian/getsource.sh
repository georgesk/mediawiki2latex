#!/bin/sh

if ! dh_testdir; then
    echo The command $0 should be called inside a main debian source directory.
    exit 1
fi

lastversion=$(git ls-remote git://git.code.sf.net/p/wb2pdf/git |  sed -n 's%.*refs/tags/\(v[.0-9]*\).*%\1%p' | uniq | sort | tail -1 | tr -d 'v')
echo "The version in the Git repository is $lastversion."

currentversion=$(dpkg-parsechangelog| sed -n 's/Version: \(.*\)-.*/\1/p')

if  [ "$currentversion" = "$lastversion" ] || \
    [ "$currentversion" \> "$lastversion" ] ; then
    echo "The current version is $currentversion. Giving up"
    exit 1
else
    # going to work in the parent directory
    cd ..

    git clone git://git.code.sf.net/p/wb2pdf/git mediawiki2latex-$lastversion
    cd mediawiki2latex-$lastversion
    git checkout tags/v$lastversion
    cd ..
    tar czf mediawiki2latex_$lastversion.orig.tar.gz mediawiki2latex-$lastversion --exclude=.git --exclude=.gitignore

    # clean the temporary dir
    rm -rf mediawiki2latex-$lastversion

    echo "Done: made the new archive mediawiki2latex_$lastversion.orig.tar.gz"
fi
